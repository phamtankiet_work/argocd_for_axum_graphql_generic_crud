let pkgs = import <nixpkgs> { };
in
pkgs.mkShell {
  buildInputs = with pkgs; [
    zsh-nix-shell
    zsh-autocomplete
    zsh-autosuggestions
    zsh-syntax-highlighting
    kubectl
    kubernetes-helm
  ];
}